package com.morrisonexpress.jpa;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;



@NoRepositoryBean
public interface BaseRepository<T,ID extends Serializable> {

	public T add(T entity);
	
	public T update(T entity);
	
	public T enable(T entity);
	
	public T disable(T entity);

	void deleteAll();

	void delete(T o);
	
	T getById(long id);
	
}