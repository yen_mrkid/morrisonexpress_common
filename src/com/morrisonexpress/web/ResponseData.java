package com.morrisonexpress.web;

public class ResponseData<T>
{
	public T data;
	public String errorMessage;
	public String resultMessage;
	
	public ResponseData(){
		this.resultMessage="success";
	}
}